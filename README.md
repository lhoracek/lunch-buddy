# LunchBuddy

Ukázková aplikace pro Android workshop [Etnetera](http://www.etnetera.cz)

*     Gradle
*     Fragments
*     Dagger
*     Otto
*     ButterKnife
*     Retrofit
*     Crouton
*     ViewBadger
*     ImageLoader
*     SugarORM
*     Google Cloud Messaging
*     Google Account Authentication

Může být použito i jako základ k jiným projektům